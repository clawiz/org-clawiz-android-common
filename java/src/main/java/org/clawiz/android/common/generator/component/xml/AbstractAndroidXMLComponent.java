/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.common.generator.component.xml;

import org.clawiz.android.common.generator.AbstractAndroidGenerateContext;
import org.clawiz.android.common.generator.AndroidGenerator;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.xml.component.AbstractXMLComponent;

public class AbstractAndroidXMLComponent extends AbstractXMLComponent {

    AndroidGenerator androidGenerator;

    public AndroidGenerator getAndroidGenerator() {
        return androidGenerator;
    }

    public void setAndroidGenerator(AndroidGenerator androidGenerator) {
        this.androidGenerator = androidGenerator;
    }


    public AbstractAndroidGenerateContext getGenerateContext() {
        return getAndroidGenerator().getContext();
    }

    @Override
    public void prepare() {
        super.prepare();
        setAndroidGenerator((AndroidGenerator) getGenerator());
    }
}
