/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.common.generator;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.session.Session;

public interface AndroidGenerateContext {

    MetadataNode getMetadataNode();

    void setMetadataNode(MetadataNode metadataNode);

    String getPackageName();
    void setPackageName(String packageName);

    String getStudioProjectPath();
    void setStudioProjectPath(String studioProjectPath);

    public void prepare(Session session) throws CoreException;

}
