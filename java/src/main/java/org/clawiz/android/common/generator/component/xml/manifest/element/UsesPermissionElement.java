/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.common.generator.component.xml.manifest.element;

import org.clawiz.android.common.generator.component.xml.AbstractAndroidXMLElement;
import org.clawiz.core.common.CoreException;

public class UsesPermissionElement extends AbstractAndroidXMLElement {

    String permissionName;

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    @Override
    public String getTagName() {
        return "uses-permission";
    }


    @Override
    public void process() {
        super.process();
    }
}
