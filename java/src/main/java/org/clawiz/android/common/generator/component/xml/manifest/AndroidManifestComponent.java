/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.common.generator.component.xml.manifest;

import org.clawiz.android.common.generator.component.xml.AbstractAndroidXMLComponent;
import org.clawiz.android.common.generator.component.xml.manifest.element.ApplicationElement;
import org.clawiz.android.common.generator.component.xml.manifest.element.ManifestElement;
import org.clawiz.android.common.generator.component.xml.manifest.element.UsesPermissionElement;
import org.clawiz.core.common.CoreException;

public class AndroidManifestComponent extends AbstractAndroidXMLComponent {

    @Override
    public void prepare() {
        super.prepare();

        setFileExistsMode(FileExistsMode.MERGE);
        setTargetPath(getAndroidGenerator().getMainPath());
        setFileName("AndroidManifest.xml");

        ManifestElement manifest = addElement(ManifestElement.class);

        manifest.addElement(UsesPermissionElement.class).setPermissionName("android.permission.INTERNET");

        ApplicationElement application = manifest.addElement(ApplicationElement.class);

    }

    @Override
    public void write() {
//        super.write();
    }
}
