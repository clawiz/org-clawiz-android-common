/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.common.generator;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.installer.parser.file.FileParser;

import java.io.File;

public class AndroidGenerator extends AbstractGenerator {


    AbstractAndroidGenerateContext context;

    public AbstractAndroidGenerateContext getContext() {
        return context;
    }

    public void setContext(AbstractAndroidGenerateContext context) {
        this.context = context;
    }

    public String getPackageName() {
        return context.getPackageName();
    }

    public String getStudioProjectPath() {
        return context.getStudioProjectPath();
    }

    public String getAppPath() {
        return getStudioProjectPath() + File.separator + "app";
    }

    public String getSrcPath() {
        return getAppPath() + File.separator + "src";
    }

    public String getMainPath() {
        return getSrcPath() + File.separator + "main";
    }

    public String getJavaPath() {
        return getMainPath() + File.separator + "java";
    }

    public String getResPath() {
        return getMainPath() + File.separator + "res";
    }

    public String getResLayoutPath() {
        return getResPath() + File.separator + "layout";
    }

    @Override
    public MetadataNode getMetadataNode() {
        return getContext().getMetadataNode();
    }
}
